/*global gantt*/
import React, { Component } from 'react';
import moment from 'moment'
import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import '../node_modules/dhtmlx-gantt/codebase/sources/locale/locale_th'
//  import '../node_modules/dhtmlx-gantt/codebase/ext/dhtmlxgantt_grouping'
// import 'dhtmlx-gantt/codebase/locale/locale_th.js';

export default class Gantt extends Component {
  setZoom(value){
    switch (value){
      case 'Hours':
        gantt.config.scale_unit = 'day';
        gantt.config.date_scale = '%d %M';
        gantt.config.scale_height = 60;
        gantt.config.min_column_width = 30;
        gantt.config.subscales = [
          {unit:'hour', step:1, date:'%H'}
        ];
        break;
      case 'Days':
        gantt.config.min_column_width = 45;
        gantt.config.scale_unit = "week";
        gantt.config.date_scale = "สัปดาห์ที่ %W";
        gantt.config.subscales = [
          {unit: "day", step: 1, date: "%D"},
          {unit: "day", step: 1, date: "%d/%m/%y"},
        ];
        gantt.config.scale_height = 60;
        break;
      case 'Months':
        gantt.config.min_column_width = 70;
        gantt.config.scale_unit = "month";
        gantt.config.date_scale = "%F";
        gantt.config.scale_height = 60;
        gantt.config.subscales = [
          {unit:"week", step:1, date:"#%W"}
        ];
        break;
      default:
        break;
    }
  }

  shouldComponentUpdate(nextProps){
    return this.props.zoom !== nextProps.zoom || this.props.tasks != nextProps.tasks
  }

  componentDidUpdate() {
    gantt.render();
    gantt.refreshData();
    gantt.parse(this.props.tasks);
    
  }
  
  renderData() {
    gantt.config.columns = [
      {name:"name",       label:"ชื่องาน",  width:"*", tree:true },
      {name:"end_date", label:"วันส่งงาน", align: "center", format: '' },
      {name:"duration",   label:"ระยะเวลา",   align: "center" },
      {name:"add",        label:"",           width:44 }
    ]

    gantt.templates.task_text=function(start,end,task){
       return `ชื่องาน: ${task.name}, ผู้รับผิดชอบ ${task.holder}`
    };
    
    gantt.templates.date_grid = function(date, task){
      if( task.end_date) {
        const subDate = gantt.calculateEndDate(task.start_date,task.duration -1 ,"day"); 
        return gantt.templates.grid_date_format(subDate)
      }
      // if(task && gantt.isUnscheduled(task) && gantt.config.show_unscheduled){
      //      return gantt.templates.task_unscheduled_time(task);
      //  }else{
      //      return gantt.templates.grid_date_format(date);
      // }
   }

    gantt.templates.task_class = (start, end, task) => {
      switch(task.$level) {
        case  0:
          return 'parent_task'
        case 1:
          return 'parent_task'
        default:
          break;
      }
    }

    gantt.templates.grid_row_class = function(start, end, task){
      if(task.$level > 1){
         return "nested_task"
      } else {
        return "parent_task_side"
      }
      return "";
   };
    gantt.attachEvent('onAfterTaskAdd', (id, task) => {
      if(this.props.onTaskUpdated) {
        this.props.onTaskUpdated(id, 'inserted', task);
      }
    });

    gantt.attachEvent('onAfterTaskUpdate', (id, task) => {
      if(this.props.onTaskUpdated) {
        this.props.onTaskUpdated(id, 'updated', task);
      }
    });

    gantt.attachEvent('onAfterTaskDelete', (id) => {
      if(this.props.onTaskUpdated) {
        this.props.onTaskUpdated(id, 'deleted');
      }
    });

    gantt.attachEvent('onAfterLinkAdd', (id, link) => {
      if(this.props.onLinkUpdated) {
        this.props.onLinkUpdated(id, 'inserted', link);
      }
    });

    gantt.attachEvent('onAfterLinkUpdate', (id, link) => {
      if(this.props.onLinkUpdated) {
        this.props.onLinkUpdated(id, 'updated', link);
      }
    });

    gantt.attachEvent('onAfterLinkDelete', (id, link) => {
      if(this.props.onLinkUpdated) {
        this.props.onLinkUpdated(id, 'deleted');
      }
    });
    gantt.attachEvent('onLightboxButton', (button_id, node, e) => {
      switch (button_id) {
        case 'add_template_btn':
          gantt.resetLightbox()
          gantt.hideCover()
          this.props.showTemplateModal()
          break;
        case 'add_holder_btn':
          gantt.resetLightbox()
          gantt.hideCover()
          this.props.showHolderModal()
          break;
        default:
          break;
      }
    })
    gantt.attachEvent('onBeforeLightbox', (id) => {
      const task = gantt.getTask(id)
      if(task.isCompany || task.isParent){
        this.props.showEditCompanyModal(id)
      } else {
        return true
      }
      
    });

    gantt.attachEvent('onTaskLoading', (task) => {
       task.isParent = task.isCompany
       return true
    })

    gantt.form_blocks['select_duration'] = {
      render: (sns) => {
        return '<div class="dhx_cal_ltext" style="padding: 10px;" > <input type="number" id="input_durations" /></div>'
      },
      set_value: (node, value, task, section) => {
        node.childNodes[1].value = task.duration || ''
      },
      get_value: (node, task, section) => {
        return parseInt(node.childNodes[1].value)
      },
      focus: (node) => {
        const a = node.childNodes[1];
        a.focus();
      }
    }
    const opts = this.props.task_template.map((data, index) => {
      return {key: data.name, label: data.name}
    })
    const handleSelectTemplate = (event) => {
      const name = event.target.value
      const obj = this.props.task_template.filter((d) => d.name === name )
      if(obj.length === 1) {
        const data = obj[0]
        document.getElementById('input_durations').max = data.maxDuration
      }
    }
    const holders = this.props.holders.map((data, index) => {
      return { key: data.name, label: data.name }
    })

    gantt.config.lightbox.sections = [
      { name:"template", height:22, map_to:"template",type:"select",options:opts , onchange: (e) => handleSelectTemplate(e) },
      { name:"task_name", height:30, map_to:"name", type:"textarea", focus:true},
      { name:"description", height:40, map_to:"description", type:"textarea"},
      { name: 'holder',  map_to: 'holder', type: 'select', options: holders },
      { name:"start_date", height:40, type:"time", map_to:"start_date", single_date: true, year_range: [ 2559, 2580] },
      { name: "duration" , type: "select_duration", map_to: "duration_amount" }                         
    ]

    gantt.config.buttons_left = ["dhx_save_btn", "dhx_cancel_btn", "add_template_btn", "add_holder_btn"];
    gantt.config.buttons_right = ["dhx_delete_btn"];
    gantt.config.open_tree_initially = true;
    gantt.config.work_time = true;
    gantt.config.skip_off_time = true;
    gantt.config.order_branch = true;
    gantt.templates.scale_cell_class = function(date){
      if(date.getDay()==0||date.getDay()==6){
          return "weekend";
      }
  };
  gantt.templates.task_cell_class = function(item,date){
      if(date.getDay()==0||date.getDay()==6){ 
          return "weekend" ;
      }
  };
  }
  componentDidMount() {
    this.renderData()
    gantt.init(this.ganttContainer);
    gantt.parse(this.props.tasks);
  }
  print = () => {
    gantt.exportToPDF()
  }
  render() {
    this.setZoom(this.props.zoom);
    return (
      <div
      if="main_data"
      ref={(input) => { this.ganttContainer = input }}
      style={{width: '100%', height: '100%'}}
         >
  </div>
    );
  }
}