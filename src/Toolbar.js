import React, { Component } from 'react';

export default class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.handleZoomChange = this.handleZoomChange.bind(this);
  }

  handleZoomChange(e) {
    if(this.props.onZoomChange){
      this.props.onZoomChange(e.target.value)
    }
  }
  print = () => {
  document.getElementById('print-btn').click()
  }
  showCompanyModal = () => {
    this.props.showCompanyModal()
  }
  render() {
    let zoomRadios = [
      {label: 'ชั่วโมง', value: 'Hours'},
      { label: 'วัน', value: 'Days'},
      { label: 'เดือน', value: 'Months'}
    ].map((data) => {
      let isActive = this.props.zoom === data;
      return (
        <label key={data.value} className={`radio-label ${isActive ? 'radio-label-active': ''}`}>
          <input type='radio'
             checked={isActive}
             onChange={this.handleZoomChange}
             value={data.value}/>
          {data.label}
        </label>
      );
    });

    return (
      <div className="zoom-bar">
        <b>มุมมอง: </b>
          {zoomRadios}
        <b className="manage-label" >จัดการ: </b>
          <button className="print-btn"  onClick={ this.showCompanyModal }>เพิ่มบริษัท</button>
          <button className="print-btn"  onClick={ this.print }>Print</button>
      </div>
    );
  }
}
