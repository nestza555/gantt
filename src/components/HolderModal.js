import React from 'react';
import axios from 'axios';
const host = 'https://api.nestped.tk/api'

export default class HolderModal extends React.Component {
  state = {
    name: '',
    holders: []
  }
  componentDidMount () {
    this.getData()
  }

  getData() {
    axios.get(host+'/holder')
    .then(res => res.data)
    .then(res => this.setState({ holders: res }))
  }

  handleSubmit = () => {
    this
      .props
      .addNewHolder(this.state.name)
  }
  handleClose = () => {
    this.props.showHolderModal()
  }
  removeHolder (id) {
    axios.delete(host+`/holder/${id}`)
    .then(res => this.getData())
    .catch(err => alert(err.message))
  }
  renderHolders () {
   return this.state.holders.map((holder, index) => {
     return (
       <div className="holder-item" key={ index }>
        <span>{ holder.name} <button onClick={ this.removeHolder.bind(this, holder._id) }>ลบ</button></span>
       </div>
     )
   })
  }
  render() {
    if (this.props.show === true) {
      return (
        <div className="modal-cover">
          <div className="template-modal">
          <button className="close-btn" onClick={ this.handleClose }>close</button>
          <h2 className="title">เพิ่มผู้รับผิดชอบงาน</h2>
          <input
            type="text"
            placeholder="ชื่อผู้รับผิดชอบ"
            className="input_task"
            value={this.state.name}
            onChange={(e) => this.setState({name: e.target.value})}/>
          <br/>
          <br/>
          <button className="submit_task_btn" onClick={this.handleSubmit}>
            บันทึก
          </button>
          <div className="holders-items">
            { this.renderHolders() }
          </div>
        </div>
        </div>
      )
    } else {
      return (
        <div></div>
      )
    }
  }
}