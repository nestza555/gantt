import React from 'react';
import axios from 'axios'
import {CirclePicker} from 'react-color';
const host = 'https://api.nestped.tk/api'

export default class TemplateModal extends React.Component {
  state = {
    color: '',
    name: '',
    maxDuration: '',
    templates: []
  }
  componentDidMount () {
    this.getData()
  }

  getData() {
    axios.get(host+'/template')
    .then(res => res.data)
    .then(res => this.setState({ templates: res }))
  }

  handleSelectColor = (color, event) => {
    this.setState({color: color.hex})
  }
  handleSubmit = () => {
    this
      .props
      .addNewTemplate(this.state)
  }
  handleClose = () => {
    this.props.showTemplateModal()
  }
  
  removeTemplate (id) {
      axios.delete(host+`/template/${id}`)
        .then(res => this.getData())
        .catch(err => alert(err.message))
  }
  renderTemplates () {
    return this.state.templates.map((template, index) => {
      return (
        <tr  key={ index }>
          <td>{ template.name }</td>
          <td>{ template.maxDuration }</td>
          <td><button className="btn-danger" onClick={this.removeTemplate.bind(this, template._id)}>ลบ</button></td>
        </tr>
      )
    })
  }
  render() {
    if (this.props.show === true) {
      return (
        <div className="modal-cover">
          <div className="template-modal">
            <button className="close-btn" onClick={this.handleClose}>close</button>
            <h2 className="title">เพิ่มประเภทงาน</h2>
            <input
              type="text"
              placeholder="ชื่อประเภทงาน"
              className="input_task"
              value={this.state.name}
              onChange={(e) => this.setState({name: e.target.value})}/>
            <input
              type="number"
              placeholder="จำนวนวันสูงสุด"
              className="input_task"
              value={this.state.maxDuration}
              onChange={(e) => this.setState({maxDuration: e.target.value})}/>
            <br/>
            <div className="color-picker-wrapper">
              <CirclePicker color={this.state.color} onChange={this.handleSelectColor}/>
            </div>
            <button className="submit_task_btn" onClick={this.handleSubmit}>
              บันทึก
            </button>
            <table className="template-items">
            <thead>
              <tr>
                <th>ชื่อประเภทงาน</th>
                <th>จำนวนวันสูงสุด</th>
                <th>จัดการ</th>
              </tr>
            </thead>
            <tbody>
            { this.renderTemplates() }
            </tbody>
            </table>
          </div>
        </div>
      )
    } else {
      return (
        <div></div>
      )
    }
  }
}