import React from 'react';

export default class EditCompanyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    }
  }  

  componentWillReceiveProps(nextProps)  {
    this.setState({
      name: nextProps.data.name
    })
  }
  handleSubmit = () => {
    const { name } = this.state
    const { id } = this.props.data
    this.props.submit(name, id)
  }
  handleRemove = () => {
    this.props.remove(this.props.data.id)
  }
  handleClose = () => {
    this.props.showEditCompanyModal()
  }
  
  render() {
    if (this.props.show === true) {
      return (
        <div className="modal-cover">
          <div className="template-modal">
            <button className="close-btn" onClick={this.handleClose}>close</button>
            <h2 className="title">แก้ไขข้อมูลบริษัท</h2>
            <input
              type="text"
              placeholder="ชื่อบริษัท"
              className="input_task"
              value={ this.state.name }
              onChange={(e) => {this.setState({ name: e.target.value })}}
              ref="name"
              />
            <br/>
            <br/>
            <button className="submit_task_btn" onClick={this.handleSubmit}>
              บันทึก
            </button>
            <button className="remove_task_btn" onClick={this.handleRemove}>
            ลบ
          </button>
          </div>
        </div>
      )
    } else {
      return (
        <div></div>
      )
    }
  }
}