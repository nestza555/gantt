import React from 'react';

export default class CompanyModal extends React.Component {
  state = {
    name: ''
  }

  handleSubmit = () => {
    this.props.submit(this.state.name)
    this.setState({
      name: ''
    })
  }
  handleClose = () => {
    this.props.showCompanyModal()
  }
  
  render() {
    if (this.props.show === true) {
      return (
        <div className="modal-cover">
          <div className="template-modal">
            <button className="close-btn" onClick={this.handleClose}>close</button>
            <h2 className="title">เพิ่มบริษัท</h2>
            <input
              type="text"
              placeholder="ชื่อบริษัท"
              className="input_task"
              value={this.state.name}
              onChange={(e) => this.setState({name: e.target.value})}/>
            <br/>
            <br/>
            <button className="submit_task_btn" onClick={this.handleSubmit}>
              บันทึก
            </button>
          </div>
        </div>
      )
    } else {
      return (
        <div></div>
      )
    }
  }
}