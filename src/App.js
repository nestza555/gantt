import React, {Component} from 'react';
import axios from 'axios';
import moment from 'moment';
import md5 from 'md5'
import Gantt from './Gantt';
import Toolbar from './Toolbar';
import TemplateModal from './components/TemplateModal'
import HolderModal from './components/HolderModal'
import CompanyModal from './components/CompanyModal'
import EditCompanyModal from './components/EditCompanyModal'
import './App.css';

const host = 'https://api.nestped.tk/api'
class App extends Component {
  state = {
    currentZoom: 'Days',
    messages: [],
    task: [],
    links: [],
    task_template: [],
    holders: [],
    showTemplateModal: false,
    showHolderModal: false,
    showCompanyModal: false,
    showEditCompanyModal: false,
    companyDetails: {}
  }

  async componentDidMount() {
    this.getData()
  }
  getData = async() => {
    try {
      const tasks = await axios.get(host+'/task')
      const links = await axios.get(host+'/link')
      const templates = await axios.get(host+'/template')
      const holders = await axios.get(host+'/holder')

      this.setState({task: tasks.data, links: links.data, task_template: templates.data, holders: holders.data, pageMounted: true})
    } catch (err) {
      console.error(err)
    }
  }

  addMessage(message) {
    var messages = this
      .state
      .messages
      .slice();
    var prevKey = messages.length
      ? messages[0].key
      : 0;

    messages.unshift({
      key: prevKey + 1,
      message
    });
    if (messages.length > 40) {
      messages.pop();
    }
    this.setState({messages});
  }

  createTask = (task) => {
    const {
      id,
      holder,
      name,
      description,
      start_date,
      duration_amount,
      parent,
      template
    } = task
    const filterTemplate = this
      .state
      .task_template
      .filter((d) => d.name === template)
    const filteredTemplate = filterTemplate[0]
    const data = {
      name: name,
      description: description,
      holder: holder,
      duration: duration_amount,
      start_date: moment(start_date).format('DD-MM-YYYY'),
      template: template,
      color: filteredTemplate.color,
      id: id,
      parent: parent
    }
    axios.post(host+'/task', data)
      .then(res => this.getData())
      .catch(err => console.error(err))
  }
  updateTask = (task) => {
    const {
      _id,
      id,
      description,
      holder,
      duration,
      duration_amount,
      start_date,
      template,
      color,
      parent,
      progress,
      name
    } = task
    let duration_val = duration
    if(duration_amount) duration_val = duration_amount
    const data = {
      id,
      description,
      holder,
      duration: duration_val,
      start_date: moment(start_date).format('DD-MM-YYYY'),
      template,
      color,
      parent,
      progress,
      name
    }
    axios.post(host+`/task/${id}`, data)
      .then(res => this.getData())
      .catch(err => console.error(err))
  }
  removeTask = (id) => {
    axios
      .delete(host+`/task/${id}`)
      .then(res => this.getData())
      .catch(err => console.error(err))
  }
  onTaskUpdated = (id, mode, task) => {
    switch (mode) {
      case 'inserted':
        this.createTask(task);
        break;
      case 'updated':
        this.updateTask(task);
        break;
      case 'deleted':
        this.removeTask(id);
        break;
      default:
        break;
    }
  }

  createLink = (link) => {
    axios
      .post(host+`/link`, link)
      .then(res => res.data)
      .then(links => this.setState({links}))
      .catch(err => console.error(err))
  }
  updateLink = (id, link) => {
    axios
      .post(host+`/link/${id}`, link)
      .then(res => this.getData())
      .catch(err => console.error(err))
  }
  deleteLink = (id) => {
    axios
      .delete(host+`/link/${id}`)
      .then(res => this.getData())
      .catch(err => console.error(err))
  }

  onLinkUpdated = (id, mode, link) => {
    switch (mode) {
      case 'inserted':
        this.createLink(link);
        break
      case 'updated':
        this.updateLink(id, link);
        break
      case 'deleted':
        this.deleteLink(id);
        break
      default:
        break
    }
  }

  handleZoomChange = (zoom) => {
    this.setState({currentZoom: zoom});
  }
  handleAddTask() {
    this.setState({
      task: [
        ...this.state.task, {
          id: this.state.task.length,
          text: 'Task #3',
          start_date: '18-04-2017',
          duration: 5,
          progress: 0.4
        }
      ]
    })
  }

  showCompanyModal = () => {
    this.setState({
      showCompanyModal: !this.state.showCompanyModal
    })
  }
  renderGantt() {
    if (this.state.pageMounted === true) {
      return (<Gantt
        tasks={{
        data: this.state.task,
        links: this.state.links
      }}
        zoom={this.state.currentZoom}
        onTaskUpdated={this.onTaskUpdated}
        onLinkUpdated={this.onLinkUpdated}
        task_template={this.state.task_template}
        holders={this.state.holders}
        showTemplateModal={this.showTemplateModal}
        holderModalStatus={ this.state.showHolderModal }
        showEditCompanyModal={ this.showEditCompanyModal }
        showHolderModal={this.showHolderModal}/>)
    }
  }
  showTemplateModal = () => {
    this.setState({
      showTemplateModal: !this.state.showTemplateModal
    })
  }
  showHolderModal = () => {
    this.setState({
      showHolderModal: !this.state.showHolderModal
    })
  }
  addNewTemplate = (data) => {
    const {color, name, maxDuration} = data
    if (name.length === '' && color.length === '' && maxDuration === '') {
      alert('กรุณากรอกข้อมูลให้ครบ')
      return
    }
    axios
      .post(host+'/template' , data)
      .then(res => window.location.reload())
      .catch(err => console.error(err))
  }
  addNewHolder = (data) => {
    if (data === '') {
      alert('กรุณากรอกชื่อ')
      return
    }
    axios
      .post(host+'/holder', {name: data})
      .then(res => window.location.reload())
      .catch(err => console.error(err))
  }
  
  onSubmitCompany = (name) => {
    const id = md5(name) + new Date().getUTCMilliseconds(); // id of task can adjust by your self
    axios
      .post(host+'/task', { id, name, isCompany: true, start_date: '11-08-2560' })
      .then(res => {
        this.getData()
        this.setState({ showCompanyModal: false })
      })
      .catch(err => alert(err.message))
  }
  getCompanyData = (id) => {

  }
  showEditCompanyModal = (id) => {
    axios
    .get(host+`/task/${id}`)
    .then(res => res.data)
    .then(res => this.setState({ companyDetails: res, showEditCompanyModal: !this.state.showEditCompanyModal }))
    .catch(err => alert(err.message))
  }

  updateCompany = (name,id) => {

    axios.post(host+`/task/${id}`, { name, isCompany: true })
      .then(res => this.getData())
      .then(() => this.setState({ showEditCompanyModal: false }))
      .catch(err => alert(err.message))
  }

  removeCompany = (id) => {
    axios
    .delete(host+`/task/${id}`)
    .then(res => window.location.reload())
    .catch(err => console.error(err))
  }

  render() {
    return (
      <div>
        <Toolbar zoom={this.state.currentZoom} onZoomChange={this.handleZoomChange} showCompanyModal={ this.showCompanyModal } />
        <div className="gantt-container">
          {this.renderGantt()}
        </div>
        <TemplateModal
          show={this.state.showTemplateModal}
          addNewTemplate={this.addNewTemplate}
          showTemplateModal={ this.showTemplateModal }
          />
        <HolderModal
          show={this.state.showHolderModal}
          addNewHolder={this.addNewHolder}
          showHolderModal={ this.showHolderModal }
          />
        <CompanyModal 
          show={ this.state.showCompanyModal }
          submit={ this.onSubmitCompany }
          showCompanyModal={ this.showCompanyModal }
        />
        <EditCompanyModal 
          show={ this.state.showEditCompanyModal }
          submit={ this.updateCompany }
          remove={ this.removeCompany }
          data={ this.state.companyDetails }
          showEditCompanyModal={ this.showEditCompanyModal }
        />
      </div>
    );
  }
}
export default App;
